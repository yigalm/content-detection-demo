import 'cypress-file-upload';
const  testFile = 'CompanyContactList.xlsx';

describe('Scan IT Tests', () => {
    beforeEach(() => {
        cy.visit('http://localhost:4200/')
      })

      it("Navigate to ScanI IT", () => {
        cy.get(':nth-child(1) > a')
        .click()
        .should("have.class", "active")
        cy.get('.side-menu-container > :nth-child(2) > a')
        .should("not.have.class", "active");
    })

    it("Uplaod a file to scan", () => {
        cy.get('[data-cy="uploadfilecontainer"]').attachFile(testFile);
        // cy.get('[data-cy="uploadfilecontainer"]').attachFile(testFile, { subjectType: 'drag-n-drop' });
        cy.wait(500);
        var detectorResult = cy.get('.detector-result > .name').click();
        
        cy.get('*[class^="mat-dialog"]').then($dialog => {
            
            if ($dialog.is(":visible")) {
                assert.isOk("dialog opened")
            } else {
                assert.isNotOk ("dialog was not opened")
            }
        })
 
    })
    
})