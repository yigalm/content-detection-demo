describe('Navigation Tests', () => {
    beforeEach(() => {
        cy.visit('http://localhost:4200/')
      })

    it("Navigate to Performance", () => {
        cy.get('.side-menu-container > :nth-child(2) > a')
        .click()
        .should("have.class", "active")
        cy.get(':nth-child(1) > a')
        .should("not.have.class", "active");
    })

    it("Navigate to ScanI IT", () => {
        cy.get(':nth-child(1) > a')
        .click()
        .should("have.class", "active")
        cy.get('.side-menu-container > :nth-child(2) > a')
        .should("not.have.class", "active");
    })
    
})