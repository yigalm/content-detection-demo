import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DetectorRequest, DetectorResult } from '../models/detector-result';

@Injectable({
  providedIn: 'root'
})
export class DetectorNotificationService {

  private publishDetectorRequestSubject = new BehaviorSubject<DetectorRequest>(null);
  onPublishDetectorRequest = this.publishDetectorRequestSubject.asObservable();

  publishDetectorRequest(request: DetectorRequest){
    this.publishDetectorRequestSubject.next(request);
  }

  constructor() { }
}
