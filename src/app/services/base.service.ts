import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpParams } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const headerOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class BaseService implements HttpInterceptor {
  constructor(public http: HttpClient) { }

  /*
    Intercept an outgoing HttpRequest
    check if user have token and add it to the header request
  */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    // console.log('intercept: ' + request.url);
    const access_token = localStorage.getItem('access_token');
    if (access_token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${access_token}`
        }
      });
    }

    return next.handle(request);
  }

  post(endpoint: string, body: any) : Observable <any> {    
    
    return this.http.post<any>(endpoint, body)
    .pipe(map(res => {
        
      return res.data;
    }), catchError(this.handleError<any>('post')));
  }

  get(endpoint: string) : Observable <any> {
    
    return this.http.get<any>(endpoint)
    .pipe(map(res => res.data), catchError(this.handleError<any>('get')));
  }

/**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  public handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // console.log('ERROR!!!!!!')
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
