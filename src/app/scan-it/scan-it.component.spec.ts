import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanItComponent } from './scan-it.component';

describe('ScanItComponent', () => {
  let component: ScanItComponent;
  let fixture: ComponentFixture<ScanItComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScanItComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanItComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
