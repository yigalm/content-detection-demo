import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';


@Directive({
  selector: '[appDragNdrop]'
})
export class DragNdropDirective {

  constructor() { }

  @Output() onFileDropped = new EventEmitter<any>();
	
  @HostBinding('style.background-color') private background = '#f2f8fd'
  @HostBinding('style.opacity') private opacity = '1'
	
  //Dragover listener
  @HostListener('dragover', ['$event']) onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eff8ff'; 
    this.opacity = '0.8'
  }
	
  //Dragleave listener
  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#f5fcff'
    this.opacity = '1'
  }
	
  //Drop listener
  @HostListener('drop', ['$event']) public async ondrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    
    const items = evt.dataTransfer.items;
    // console.log(evt.dataTransfer);
    
    let filesList = [];

    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        if (item.kind === 'file') {
            const entry = item.webkitGetAsEntry();
            
            
            if (entry.isFile) {
                
                let fileEntry = await this.parseFileEntry(entry);
                
                filesList.push(fileEntry);
                
            } else if (entry.isDirectory) {
              let dirctoryEntry = await this.parseDirectoryEntry(entry) as Array<any>;

              if (dirctoryEntry.length > 200){
                this.onFileDropped.emit([]);
                throw 'too many files..';
              }
              
              for(let entry of dirctoryEntry)
              {
                if (entry.isFile) {
                  let fileEntry = await this.parseFileEntry(entry);
                  
                  filesList.push(fileEntry);
  
                }
              }
            }
        }
    }
    this.background = '#f2f8fd'
    this.opacity = '1'
        
    this.onFileDropped.emit(filesList);
    /**
    let files = evt.dataTransfer.files;

    if (files.length > 0) {
      // this.onFileDropped.emit(files);
      this.onFileDropped.emit(filesList);

    }
     
     */
  }

  parseFileEntry(fileEntry) {
    return new Promise((resolve, reject) => {
        fileEntry.file(
            file => {
                resolve(file);
            },
            err => {
                reject(err);
            }
        );
    });
  }

  parseDirectoryEntry(directoryEntry) {
    const directoryReader = directoryEntry.createReader();
    return new Promise((resolve, reject) => {
        directoryReader.readEntries(
            entries => {
                resolve(entries);
            },
            err => {
                reject(err);
            }
        );
    });
}
}
