import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { DragNdropDirective } from './directives/drag-ndrop.directive';
import { FileUploadComponent } from './shared/file-upload/file-upload.component';
import { BaseService } from './services/base.service';
import { AnalyzerResultComponent } from './shared/analyzer-result/analyzer-result.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxJsonViewerModule } from 'ngx-json-viewer';

// import { MatSidenavModule } from '@angular/material/sidenav';
// import { MatDialogModule } from '@angular/material/dialog';

import { AnalyzerResultDetailsComponent } from './shared/analyzer-result-details/analyzer-result-details.component';
import { FormsModule } from '@angular/forms';

import {MaterialModule} from './material-module';
import { CustomerListComponent } from './shared/customer-list/customer-list.component';
import { ScanItComponent } from './scan-it/scan-it.component';
import { ScanningResultComponent } from './shared/scanning-result/scanning-result.component';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SettingsComponent,
    DragNdropDirective,
    FileUploadComponent,
    AnalyzerResultComponent,
    AnalyzerResultDetailsComponent,
    CustomerListComponent,
    ScanItComponent,
    ScanningResultComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxJsonViewerModule,
    MaterialModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseService,
      multi: true,
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AnalyzerResultDetailsComponent
  ]
})
export class AppModule { }
