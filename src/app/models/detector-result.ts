import { Observable } from 'rxjs';

export interface DetectorResultData {
    detectorsCount: Number,
    totalMatchCount: Number,
    detectorsId: String,
    detectors: Array<String>,
    snippets: Array<any>,
    metric: any,
}

export interface DetectorResult {
    file: String,
    customer: String,
    data: any
}
export interface DetectorRequest {
    file: String,
    customer: String,
    observer: Observable<any>
}