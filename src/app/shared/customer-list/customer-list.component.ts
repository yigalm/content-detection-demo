import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { BaseService } from 'src/app/services/base.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit, OnDestroy {

  @Output() customer_changed_event: EventEmitter<any> = new EventEmitter<any>();

  customersList: Array<string> = [];

  customerName: string;

  constructor(private dlpService: BaseService) { }

  subscription: Subscription = new Subscription();

  ngOnInit(): void {

    this.dlpService.get("http://localhost:8081/customers").subscribe(customer => {
        customer.forEach(cst => {
          this.customersList.push(cst);
        })
        
        this.customerName = customer[0];

        this.customerChanged();
        
    })

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  customerChanged(){
    
    this.customer_changed_event.emit(this.customerName);
  }

}
