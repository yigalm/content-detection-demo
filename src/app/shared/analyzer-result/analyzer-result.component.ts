import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DetectorResult } from 'src/app/models/detector-result';
import { DetectorNotificationService } from 'src/app/services/detector-notification.service';
import { AnalyzerResultDetailsComponent } from '../analyzer-result-details/analyzer-result-details.component';

@Component({
  selector: 'app-analyzer-result',
  templateUrl: './analyzer-result.component.html',
  styleUrls: ['./analyzer-result.component.scss']
})
export class AnalyzerResultComponent implements OnInit {

  results: Array<DetectorResult> = [];

  constructor(private dlpNotification: DetectorNotificationService,
              private dialog: MatDialog,
              
              ) { }

  ngOnInit() {

    this.dlpNotification.onPublishDetectorRequest.subscribe(request => {
      if (!request) return;

      let detectorResult: DetectorResult = {
        file: request.file,
        customer: request.customer,
        data: null
      }

      this.results.unshift(detectorResult);

      request.observer.subscribe(results => {
        detectorResult.data = results;
      })
    })
  }

  showResultDetails(result){
    const dialogRef = this.dialog.open(AnalyzerResultDetailsComponent, {
      // width: '250px',
      data: {
        details: result
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed->' + result);
    });
  }

  deleteAttachment(index) {
    this.results.splice(index, 1);
  }

}
