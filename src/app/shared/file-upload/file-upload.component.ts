import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DetectorRequest } from 'src/app/models/detector-result';
import { BaseService } from 'src/app/services/base.service';
import { DetectorNotificationService } from 'src/app/services/detector-notification.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  extractEmbedded: boolean = true;

  customerName: string;
  currentFile: File;

  constructor(private dlpService: BaseService,
              private dlpNotification: DetectorNotificationService) { }

  ngOnInit() {
  }

  async uploadFile(event) {
    
    for (let index = 0; index < event.length; index++) {
      let file: File = event[index];
      
      this.currentFile = file;
      this.analyzeFile(this.currentFile, this.customerName);

    }
  }

  analyzeFile(file: File, customerName: string){
    
    if (!file) return;

    let formData = new FormData();
      
    formData.set('file', file, file.name);
    formData.set('customer', customerName);
    formData.set('extractEmbedded', this.extractEmbedded.toString());

    const dlpObserverRequest = new Observable( (observer )=> {
      
      this.dlpService.post("http://localhost:8081/analyze-file", formData).subscribe(res => {

        observer.next(res);
        
      });

    });

    let dlpRequest: DetectorRequest = {
      file: file.name,
      customer: customerName,
      observer: dlpObserverRequest,
    }
    this.dlpNotification.publishDetectorRequest(dlpRequest);
  }

  customer_changed_event(customer){
    this.customerName = customer;
  }

  refreshAnalyze(){
    this.analyzeFile(this.currentFile, this.customerName);
    
  }

  refreshTooltip(){
    if (this.currentFile){
      return `click to re-analyze ${this.currentFile.name}`
    }

    return 'please pick file to analyze';
  }

  clearAll(){
    
  }

}
