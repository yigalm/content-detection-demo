import { isDataSource } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DetectorResult, DetectorResultData } from 'src/app/models/detector-result';
import { DetectorNotificationService } from 'src/app/services/detector-notification.service';
import { AnalyzerResultDetailsComponent } from '../analyzer-result-details/analyzer-result-details.component';

@Component({
  selector: 'app-scanning-result',
  templateUrl: './scanning-result.component.html',
  styleUrls: ['./scanning-result.component.scss']
})
export class ScanningResultComponent implements OnInit {

  results: Array<DetectorResult> = [];

  constructor(private dlpNotification: DetectorNotificationService,
              private dialog: MatDialog,
              
              ) { }

  ngOnInit() {

    this.dlpNotification.onPublishDetectorRequest.subscribe(request => {
      if (!request) return;

      let result: DetectorResult = {
        file: request.file,
        customer: request.customer,
        data: null
      }

      this.results.unshift(result);

      request.observer.subscribe(results => {
        // console.log(results);

        let dlpResult = results.dlpResult;
        let metric = results.metric;

        let totalMatch = 0;
        dlpResult.matches.forEach(match => {
          totalMatch += match.matchCount;
        });

        let ids = '';

        dlpResult.detectors.forEach(detector => {
          ids += detector.detectorId + ', ';
        });

        if (ids.endsWith(', ')){
          ids = ids.slice(0, ids.length - 2);
        }

        let data: DetectorResultData = {
          detectorsCount: dlpResult.detectors.length,
          detectorsId: ids,
          totalMatchCount: totalMatch,
          detectors: dlpResult.detectors,
          snippets: dlpResult.snippets,
          metric: metric
        }

        result.data = data;
      })
    })
  }

  showResultDetails(event, result, mode){
    event.preventDefault();
    const dialogRef = this.dialog.open(AnalyzerResultDetailsComponent, {
      // width: '250px',
      data: {
        details: result,
        mode: mode
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed->' + result);
    });
  }


  deleteAttachment(index) {
    this.results.splice(index, 1);
  }

  getSnippets(snippetsResults){
    let snippetsStr: Array<any> = [];

    Object.keys(snippetsResults).forEach(key => {
      let snpArray:Array<any> = snippetsResults[key];
      snpArray.forEach(snippet => {
        snippetsStr.push(snippet.content);
        // console.log(snippet.content);
        
        // console.log(snippet.content[snippet.content.length - 1]);
        
      })
      
    })
    // console.log(snippetsResults);
    
    let tooltip = '';
    snippetsStr.forEach(snp => {
      tooltip += snp + '\n';
    })
    // snippetsStr.join('\n');

    return tooltip;
  }

}
