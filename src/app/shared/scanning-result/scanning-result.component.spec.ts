import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanningResultComponent } from './scanning-result.component';

describe('ScanningResultComponent', () => {
  let component: ScanningResultComponent;
  let fixture: ComponentFixture<ScanningResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScanningResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanningResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
