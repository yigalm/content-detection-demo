import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyzerResultDetailsComponent } from './analyzer-result-details.component';

describe('AnalyzerResultDetailsComponent', () => {
  let component: AnalyzerResultDetailsComponent;
  let fixture: ComponentFixture<AnalyzerResultDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalyzerResultDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyzerResultDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
