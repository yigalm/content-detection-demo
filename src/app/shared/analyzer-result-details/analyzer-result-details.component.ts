import { Component, ElementRef, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-analyzer-result-details',
  templateUrl: './analyzer-result-details.component.html',
  styleUrls: ['./analyzer-result-details.component.scss']
})
export class AnalyzerResultDetailsComponent implements OnInit {

  details: any;

  constructor(@Inject(MAT_DIALOG_DATA) data: { trigger: ElementRef, details: any, mode: string }) {
    
    let snippets: Array<any> = [];

    this.details = data.details;

    if (data.mode == 'basic'){
      Object.keys(data.details.data.snippets).forEach(key => {
        let snpArray:Array<any> = data.details.data.snippets[key];
        snpArray.forEach(snippet => {
          
          delete snippet.offset;
          // delete snippet.length;
  
          snippets.push(snippet);
        })
        
      })
      this.details = {
        file: data.details.file,
        profile: data.details.customer,
        snippets: snippets
      }
  
    }

    // console.log(data.details);
    
   }

  ngOnInit(): void {
  }

}
